import Layout from '../../components/layout'
import Head from 'next/head'
import dynamic from 'next/dynamic'
const BaiduMapComponet = dynamic(
  import('../../components/baiduMap/baidu-map'),
  {
    ssr: false,
  }
)
// https://nextjs.org/docs/advanced-features/dynamic-import

export default function BaiduMap({ postData }) {
  return (
    <Layout>
      <Head>
        <title>{"Baidu Map"}</title>
      </Head>
      <article>
        <BaiduMapComponet />
      </article>
    </Layout>
  )
}