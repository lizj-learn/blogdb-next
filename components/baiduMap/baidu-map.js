// import dynamic from 'next/dynamic'
import React from 'react'
import Script from 'next/script'
import {Map, Marker, NavigationControl, InfoWindow} from 'react-bmapgl'
// https://nextjs.org/docs/advanced-features/dynamic-import
// const Map = dynamic(
//   import('react-bmapgl/Map'),
//   {
//     ssr: false,
//   }
// )
// const Marker = dynamic(
//   import('react-bmapgl/Overlay/Marker'),
//   {
//     ssr: false,
//   }
// )
// const NavigationControl = dynamic(
//   import('react-bmapgl/Control/NavigationControl'),
//   {
//     ssr: false,
//   }
// )
// const InfoWindow = dynamic(
//   import('react-bmapgl/Overlay/InfoWindow'),
//   {
//     ssr: false,
//   }
// )
export default class BaiduMap extends React.Component {
  constructor(){
    super()
    this.state = {
      myComponent: null,
    }
    this.loadScript = this.loadScript.bind(this)
    this._setMap = this._setMap.bind(this)
  }
  render() {
    return <>
      <this.loadScript></this.loadScript>
      {
        this.state.myComponent ? <this.state.myComponent></this.state.myComponent> : 'loading...'
      }
    </>
  }
  componentDidMount() {
    if(window.BMapGL && !this.state.myComponent) {
      this._setMap()
    }
  }
  _setMap() {
    this.setState({
      myComponent: function() {
        return (
          <Map center={{lng: 116.402544, lat: 39.928216}} zoom="11">
            <Marker position={{lng: 116.402544, lat: 39.928216}} />
            <NavigationControl /> 
            <InfoWindow position={{lng: 116.402544, lat: 39.928216}} text="内容" title="标题"/>
          </Map>
        )
      }
    })
  }
  loadScript() {
    window._LoadBaiduMapScript = () => {
      this._setMap()
    }
    return <Script
      src="https://api.map.baidu.com/api?v=1.0&&type=webgl&ak=22D6kqX5biKhOh0t2gnqkANHO3zfyPZf&callback=_LoadBaiduMapScript"
    />
  }
}